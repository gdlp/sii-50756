// Mundo.cpp: implementation of the CMundo class.
// Modificado por guillermo de la peña
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <error.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

char *proyeccion;

CMundo::CMundo()
{

 	Init();
}

CMundo::~CMundo()
{
	for(int i=0;i<listaEsferas.size();i++)
 		delete listaEsferas[i];
	//close(fd); 
	munmap(pdatos,sizeof(datos));
        //close(fd_datos);

	/*close(fd_coordenadas);
	unlink("/tmp/fifoCoor");
	close(fd_teclas);
	unlink("/tmp/fifoTec");*/
}

void CMundo::InitGL()
{
 	//Habilitamos las luces, la renderizacion y el color de los materiales
 	glEnable(GL_LIGHT0);
 	glEnable(GL_LIGHTING);
 	glEnable(GL_DEPTH_TEST);
 	glEnable(GL_COLOR_MATERIAL);	

 	glMatrixMode(GL_PROJECTION);

 	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)

{

 	glDisable (GL_LIGHTING);
 	glMatrixMode(GL_TEXTURE);
 	glPushMatrix();
 	glLoadIdentity();
 	glMatrixMode(GL_PROJECTION);
 	glPushMatrix();
 	glLoadIdentity();
 	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );
 
 	glMatrixMode(GL_MODELVIEW);
 	glPushMatrix();
 	glLoadIdentity();

 	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
 	glDisable(GL_DEPTH_TEST);
 	glDisable(GL_BLEND);
 	glColor3f(r,g,b);
 	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
 	int len = strlen (mensaje );
 	for (int i = 0; i < len; i++) 
 		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
	
 	glMatrixMode(GL_TEXTURE);
 	glPopMatrix();

 	glMatrixMode(GL_PROJECTION);
 	glPopMatrix();
 	glMatrixMode(GL_MODELVIEW);
 	glPopMatrix();
 	glEnable( GL_DEPTH_TEST );
}

void CMundo::OnDraw()
{
 	//Borrado de la pantalla	
    	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

 	//Para definir el punto de vista
 	glMatrixMode(GL_MODELVIEW);	
 	glLoadIdentity();

 	gluLookAt(0.0, 0, 17,  // posicion del ojo
 		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
 		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

 	/////////////////
 	///////////
 	//		AQUI EMPIEZA MI DIBUJO

 	char cad[100];
 	sprintf(cad,"Jugador1: %d",puntos1);
 	print(cad,10,0,1,1,1);
 	sprintf(cad,"Jugador2: %d",puntos2);
 	print(cad,650,0,1,1,1);
 	int i;
 	for(i=0;i<paredes.size();i++)
 		paredes[i].Dibuja();

 	fondo_izq.Dibuja();
 	fondo_dcho.Dibuja();
 	jugador1.Dibuja();
 	jugador2.Dibuja();
	esfera.Dibuja();

 	for(int i=0;i<listaEsferas.size();i++)
 	  listaEsferas[i]->Dibuja();

 	/////////////////
 	///////////
 	//		AQUI TERMINA MI DIBUJO
 	////////////////////////////

 	//Al final, cambiar el buffer
 	glutSwapBuffers();

}

void CMundo::OnTimer(int value)
{	
 	tiempo++;
	if (tiempo==1500)
 	{
 		tiempo=0;
 		Esfera* e=new Esfera();				
		listaEsferas.push_back(e);					
 	}
 	if ((tiempo%20)==0)
 	{
 		for (int i=0;i<listaEsferas.size();i++)
 		{
 		  listaEsferas[i]->SetRadio(listaEsferas[i]->GetRadio()-0.01);		
 		}	
 	}

 	jugador1.Mueve(0.025f);
 	jugador2.Mueve(0.025f);
 	for (int i=0;i<listaEsferas.size();i++)
 		listaEsferas[i]->Mueve(0.025f);
 	int i;
 	for(i=0;i<paredes.size();i++)
 	{
 		for(int j=0;j<listaEsferas.size();j++)
 			paredes[i].Rebota(*(listaEsferas[j]));

 		paredes[i].Rebota(jugador1);
 		paredes[i].Rebota(jugador2);
 	}
 	for (int i=0;i<listaEsferas.size();i++)
 	  jugador1.Rebota(*(listaEsferas[i]));
 	for (int i=0;i<listaEsferas.size();i++)
 	  jugador2.Rebota(*(listaEsferas[i]));

 	for(int i=0;i<listaEsferas.size();i++)
 	{
 		if(fondo_izq.Rebota(*(listaEsferas[i])))		
		{			
 				listaEsferas[i]->centro.x=0;
 				listaEsferas[i]->SetRadio(0.5);
 				listaEsferas[i]->centro.y=rand()/(float)RAND_MAX;
 				listaEsferas[i]->velocidad.x=2+2*rand()/(float)RAND_MAX;
 				listaEsferas[i]->velocidad.y=2+2*rand()/(float)RAND_MAX;
 				puntos2++;	
				sprintf(cadena,"Jugador 2 marca 1 punto, lleva un total de %d puntos", puntos2);
				write(fd,cadena,sizeof(cadena));
 		}
		if(fondo_dcho.Rebota(*(listaEsferas[i])))
 		{

 				listaEsferas[i]->centro.x=0;
 				listaEsferas[i]->SetRadio(0.5);
 				listaEsferas[i]->centro.y=rand()/(float)RAND_MAX;
 				listaEsferas[i]->velocidad.x=-2-2*rand()/(float)RAND_MAX;
 				listaEsferas[i]->velocidad.y=-2-2*rand()/(float)RAND_MAX;
 				puntos1++;
				sprintf(cadena,"Jugador 1 marca 1 punto, lleva un total de %d puntos",puntos1);
				write(fd,cadena,sizeof(cadena));
 		}
 	}
	if (puntos1==3)
	{
		write(fd,"GANADOR DE LA PARTIDA -> JUGADOR 1 !!",sizeof("GANADOR DE LA PARTIDA -> JUGADOR 1 !!"));
		exit(1);
	}	
	if (puntos2==3)
	{
		write(fd,"GANADOR DE LA PARTIDA -> JUGADOR 2 !!",sizeof("GANADOR DE LA PARTIDA -> JUGADOR 2 !!"));
	exit(1);
	}	
	pdatos->raqueta1=jugador1;
	pdatos->esfera=esfera;
	if(pdatos->accion==1)             //arriba
		OnKeyboardDown('w',0,0);
	else if(pdatos->accion==-1)       //abajo
		OnKeyboardDown('s',0,0);

	//read(fd_coordenadas,cadena_cliente,sizeof(cadena_cliente));
	s_comunic.Receive(cadena_cliente,sizeof(cadena_cliente));
	sscanf(cadena_cliente,"%f %f %f %f %f %f %f %f %f %f %d %d",				&esfera.centro.x,&esfera.centro.y,&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2,&jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2,&puntos1, &puntos2);

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
 	switch(key)
 	{
 //	case 'a':jugador1.velocidad.x=-1;break;
 //	case 'd':jugador1.velocidad.x=1;break;
 	case 's':jugador1.velocidad.y=-4;break;
 	case 'w':jugador1.velocidad.y=4;break;
 	case 'l':jugador2.velocidad.y=-4;break;
 	case 'o':jugador2.velocidad.y=4;break;
 	}
	char tecla[5];
	sprintf(tecla,"%c",key);
	//write(fd_teclas,tecla,sizeof(tecla)+1);
	s_comunic.Receive(tecla,sizeof(tecla));
}

void CMundo::Init()
{
 	Esfera* e=new Esfera();
 	listaEsferas.push_back(e);	

 	Plano p;

 	//pared inferior
 	p.x1=-7;p.y1=-5;
 	p.x2=7;p.y2=-5;
 	paredes.push_back(p);

 	//superior
 	p.x1=-7;p.y1=5;
 	p.x2=7;p.y2=5;
 	paredes.push_back(p);
 	fondo_izq.r=0;
 	fondo_izq.x1=-7;fondo_izq.y1=-5;
 	fondo_izq.x2=-7;fondo_izq.y2=5;
 	fondo_dcho.r=0;
 	fondo_dcho.x1=7;fondo_dcho.y1=-5;
 	fondo_dcho.x2=7;fondo_dcho.y2=5;

 	//a la izq
 	jugador1.g=0;
 	jugador1.x1=-6;jugador1.y1=-1;
 	jugador1.x2=-6;jugador1.y2=1;

 	//a la dcha
 	jugador2.g=0;
 	jugador2.x1=6;jugador2.y1=-1;
 	jugador2.x2=6;jugador2.y2=1;

	fd_datos=open("/tmp/misDatos.txt",O_RDWR|O_CREAT|O_TRUNC,0777);	

	if((fd_datos=open("/tmp/misDatos.txt",O_RDWR|O_CREAT|O_TRUNC,0777))<0)
		perror("Error al abrir el fichero");

	write(fd_datos,&datos,sizeof(datos));	
	proyeccion=(char *)mmap(NULL, sizeof(datos), PROT_READ|PROT_WRITE, MAP_SHARED, fd_datos, 0);
	close(fd_datos);

	pdatos=(DatosMemCompartida*)proyeccion;
	pdatos->accion=0;

	/*mkfifo("/tmp/fifoCoor",0777);
	fd_coordenadas=open("/tmp/fifoCoor",O_RDONLY);
	mkfifo("/tmp/fifoTec",0777);
	fd_teclas=open("/tmp/fifoTec",O_RDONLY);*/

	printf("Introduzca su nombre:");
	gets(nombre);
	s_comunic.Connect((char*)"127.0.0.1",4800);
	s_comunic.Send(nombre, sizeof(nombre));
}
