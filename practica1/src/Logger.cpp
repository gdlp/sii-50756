#include <stdio.h>       
#include <string.h> 
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

int main()
{
	char buff[200];		
	mkfifo("/tmp/mituberia",0777);

	int fd = open("/tmp/mituberia",O_RDONLY);	

	while(read(fd,buff,sizeof(buff)))
	{
		printf("%s\n",buff);	
	}

	close(fd);
	unlink("/tmp/mituberia");
}

