#include "DatosMemCompartida.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

int main()
{
	DatosMemCompartida* pdatos;
	char* proyeccion;	
	int fd_datos;

	fd_datos=open("/tmp/datosBot.txt",O_RDWR);
	proyeccion=(char *)mmap(NULL,sizeof(*(pdatos)),PROT_WRITE|PROT_READ,MAP_SHARED,fd_datos,0);

	close(fd_datos);	

	pdatos=(DatosMemCompartida *)proyeccion;

	while(1)
	{
		usleep(25000);		
		float posicionRaqueta;
		posicionRaqueta=((pdatos->raqueta1.y2+pdatos->raqueta1.y1)/2);

		if((pdatos->esfera.centro.y)>posicionRaqueta)
			pdatos->accion=1;

		else if((pdatos->esfera.centro.y)<posicionRaqueta)
			pdatos->accion=-1;
		else
			pdatos->accion=0;

	}

	munmap(proyeccion,sizeof(*(pdatos)));
}

