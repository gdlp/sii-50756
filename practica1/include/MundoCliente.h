// Mundo.h: interface for the CMundo class.
// Modificado por Guillermo de la Peña
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"
#include "Socket.h"
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

using namespace std;

class CMundo  
{
public:
 	void Init();
	CMundo();
	virtual ~CMundo();	

 	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

 	Esfera esfera;
	vector<Esfera*> listaEsferas;
	vector<Plano> paredes;
 	Plano fondo_izq;
 	Plano fondo_dcho;
 	Raqueta jugador1;
 	Raqueta jugador2;	

	DatosMemCompartida datos;
	DatosMemCompartida* pdatos;

 	int puntos1;
	int puntos2;	
	char cadena[200];
	char cadena_cliente[200];
	int fd;
	int fd_datos;
	//int fd_teclas;
	//int fd_coordenadas;

 	int tiempo;

	char nombre[200];
	Socket s_comunic;
 };

 

 #endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
