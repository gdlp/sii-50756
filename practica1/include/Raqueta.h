// Raqueta.h: interface for the Raqueta class.
// Modificado por Guillermo de la Peña
//////////////////////////////////////////////////////////////////////

#include "Plano.h"
#include "Vector2D.h"
#pragma once

class Raqueta : public Plano  
{
 public:

 	Vector2D velocidad;
	Raqueta();
 	virtual ~Raqueta();
 	void Mueve(float t);

 };
